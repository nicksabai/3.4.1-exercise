/**
 * Server side code.
 */
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));

var Employee = {
    firstName: "James",
    lastName: "Bond",
    employeeNumber: 007,
    salary: 1000000,
    age: 35,
    department: "SIS",

    getFullName() {
        console.log("Name: " + Employee.firstName + " " + Employee.lastName);
    },

    isRich() {
        if (Employee.salary > 2000) {
            console.log("Salary: True");
            return true;
        } else {
            console.log("Salary: False");
            return false;

        }
    },

    belongsToDepartment() {
        if (typeof Employee.department === 'string') {
            console.log("Is Department a string? Yes");
            console.log("Department: " + Employee.department);
        } else {
            console.log("Is Department a string? No");
        }
    }



};

Employee.getFullName();
Employee.isRich();
Employee.belongsToDepartment();




app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});